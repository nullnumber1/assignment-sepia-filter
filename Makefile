NAME := image-transformer

CC = clang
LINKER = $(CC)
NASM = nasm
NASMFLAGS = -felf64 -g
RM = rm -rf
BUILDDIR = build
SOLUTION_DIR = solution
SRCDIR = $(SOLUTION_DIR)/src
INCDIR = $(SOLUTION_DIR)/include
OBJDIR = obj/$(SOLUTION_DIR)
CFLAGS += $(strip $(file < $(SOLUTION_DIR)/compile_flags.txt)) -I$(INCDIR) 

all: main

.PHONY: build clean
build:
	mkdir -p $(BUILDDIR)
	mkdir -p $(OBJDIR)
 
main: $(OBJDIR)/sepia.asm.o $(OBJDIR)/bmp.o $(OBJDIR)/io.o $(OBJDIR)/image.o $(OBJDIR)/transform.o $(OBJDIR)/main.o
	$(CC) -o $(BUILDDIR)/$(NAME) $^ -no-pie

$(OBJDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@
 
$(OBJDIR)/sepia.asm.o: $(SRCDIR)/sepia.asm build
	$(NASM) $(NASMFLAGS) $< -o $@

test: main
	build/./image-transformer tests/input.bmp tests/output_asm.bmp asm
	build/./image-transformer tests/input.bmp tests/output_c.bmp c

clean:
	rm -rf build/* obj/*/*.o tests/output*
