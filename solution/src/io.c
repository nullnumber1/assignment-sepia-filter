//
// Created by Artemij on 25.12.2021.
//

#include "io.h"

const char *io_return_code_string[] = {
        [OPEN_FAILED] = "Error occurred while opening the file",
        [OPEN_SUCCESS] = "File opened",
        [CLOSE_FAILED] = "Error occurred while closing the file",
        [CLOSE_SUCCESS] = "File closed",
        [FILE_CLOSED] = "Program can't deal with a closed file"
};

enum io_return_code file_open(const char *file_name, FILE **file, const char *mode) {
    *file = fopen(file_name, mode);
    if (!*file)
        return OPEN_FAILED;
    return OPEN_SUCCESS;
}

enum io_return_code file_close(FILE *file) {
    if (!file)
        return FILE_CLOSED;
    if (fclose(file))
        return CLOSE_FAILED;
    return CLOSE_SUCCESS;
}

enum io_return_code file_read(const char *file_name, FILE **file) {
    const char *mode = "r";
    return file_open(file_name, file, mode);
}

enum io_return_code file_write(const char *file_name, FILE **file) {
    const char *mode = "w";
    return file_open(file_name, file, mode);
}
