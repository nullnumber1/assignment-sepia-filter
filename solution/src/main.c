#include "image.h"
#include "bmp.h"
#include "io.h"
#include "transform.h"


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>

#define ERROR_CODE 1

static void debug(const char *message);

static void info(const char *message);

static void clean_up(struct image image, FILE *file);

int main(int argc, char *argv[]) {
    if (argc != 4) {
        debug("Check the number of args");
        return ERROR_CODE;
    }
    FILE *input = NULL, *output = NULL;
    struct image source = {0};

    enum io_return_code code = file_open(argv[1], &input, "rb");
    debug(io_return_code_string[code]);
    if (code != OPEN_SUCCESS)
        return ERROR_CODE;

    enum read_status read_status = from_bmp(input, &source);
    debug(read_status_message[read_status]);
    if (read_status != READ_OK)
        return ERROR_CODE;
        
    struct rusage r;
    struct timeval interval_start;
    struct timeval interval_end;
    long time_elapsed_millis = 0;

    if(strcmp(argv[3], "asm") == 0) {
        getrusage(RUSAGE_SELF, &r);
        interval_start = r.ru_utime;
        sepia_asm(source);
        getrusage(RUSAGE_SELF, &r);
        interval_end = r.ru_utime;
    } else {
        getrusage(RUSAGE_SELF, &r);
        interval_start = r.ru_utime;
        sepia(source);
        getrusage(RUSAGE_SELF, &r);
        interval_end = r.ru_utime;
    }
    
    time_elapsed_millis = ((interval_end.tv_sec - interval_start.tv_sec) * 1000000L) + interval_end.tv_usec - interval_start.tv_usec;

    code = file_open(argv[2], &output, "wb");
    debug(io_return_code_string[code]);
    if (code != OPEN_SUCCESS)
        return ERROR_CODE;

    enum write_status write_status = to_bmp(output, &source);
    debug(write_status_message[write_status]);
    if (write_status != WRITE_OK)
        return ERROR_CODE;

    clean_up(source, output);

    printf("Time of algorithm execution through %s is: %ld\n", argv[3], time_elapsed_millis);
    info("End with return code 0");
    return 0;
}

static void debug(const char *message) {
    fprintf(stderr, "%s\n", message);
}

static void info(const char *message) {
    printf("%s\n", message);
}

static void clean_up(struct image image, FILE *file) {
    image_destroy(&image);
    file_close(file);
}
