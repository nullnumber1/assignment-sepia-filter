//
// Created by Artemij on 27.12.2021.
//

#include "transform.h"
#include <mm_malloc.h>

static uint8_t sat(uint64_t x) { return x > 255 ? 255 : x; }

static void sepia_one(struct pixel* source) {
    static float c[3][3] = { { .131f, .543f, .272f},
                             { .168f, .686f, .349f},
                             { .189f, .769f, .393f}
    };
    struct pixel old = *source;
    source->r = sat(old.r*c[0][0] + old.g*c[0][1] + old.b*c[0][2]);
    source->g = sat(old.r*c[1][0] + old.g*c[1][1] + old.b*c[1][2]);
    source->b = sat(old.r*c[2][0] + old.g*c[2][1] + old.b*c[2][2]);
}

void sepia(struct image source) {
    size_t i, j;
    size_t w = source.width;
    size_t h = source.height;
    for (i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
            sepia_one(source.data + i*w + j);
        }
    }
}

void sepia_filter_asm(float* b_c, float* g_c, float* r_c, uint8_t* res);

void sepia_asm(struct image source) {
    size_t i, j;
    size_t w = source.width;
    size_t h = source.height;
    float b_c[4];
    float g_c[4];
    float r_c[4];
    uint8_t result[12];

    struct pixel* pixel;
    for (i=0; i < w*h; i+=4) {
        for (j=0; j < 4; j++) {
            pixel = (source.data + i + j);
            b_c[j] = pixel->b;
            g_c[j] = pixel->g;
            r_c[j] = pixel->r;
        }
        sepia_filter_asm(b_c, g_c, r_c, result);
        for (int j = 0; j < 4; j++) {
            pixel = (source.data + i + j);
            pixel->b = *(result + 3*j);
            pixel->g = *(result + 3*j + 1);
            pixel->r = *(result + 3*j + 2);
        }
    }
}
