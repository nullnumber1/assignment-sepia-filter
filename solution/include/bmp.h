//
// Created by Artemij on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include <mm_malloc.h>
#include <stdint.h>
#include <stdio.h>

#include "image.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


enum read_status {
    READ_OK = 0,
    READ_ERROR,
    SIGNATURE_INVALID,
    HEADER_INVALID,
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_CONTINUE
};

extern const char* read_status_message[];
extern const char* write_status_message[];

enum read_status from_bmp(FILE *in, struct image *image);

enum write_status to_bmp(FILE *out, const struct image *image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
